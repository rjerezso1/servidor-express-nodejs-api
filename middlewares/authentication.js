const jwt = require('jsonwebtoken')

const verifyAuth = (req, res, next) => {
    const token = req.get('token')
    jwt.verify(token, '$RTUDFFDQwos$dpo', (err, decoded) => {
        if (err) {
            return res.status(400).json({
                message: 'Usuario no válido'
            })
        }
        req.user = decoded.data
        next()
    })
} 

const verifyAdmin = (req, res, next) => {
    const rol = req.user.rol
    if (rol === 'ADMIN') {
        next()
    } else {
        return res.status(401).json({
            message: 'Usuario no válido'
        })
    }
}

module.exports = {verifyAuth, verifyAdmin}