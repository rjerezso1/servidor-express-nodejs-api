import express from 'express'
const router = express.Router()

// importar el modelo Note
import Note from '../models/note'

const { verifyAuth, verifyAdmin } = require('../middlewares/authentication')

// POST agregar una nota
router.post('/new-note', verifyAuth, async(req, res) => {
    const body = req.body
    body.userId = req.body._id
    try {
        const noteDB = await Note.create(body)
        res.status(200).json(noteDB)
    } catch (error) {
        return res.status(500).json({
            msg: 'Ocurrio un error',
            error
        })
    }  
})

//GET con parámetros
router.get('/note/:id', async(req, res) => {
    const _id = req.params.id
    try {
        const noteDB = await Note.findOne({_id})
        if (!noteDB) {
            return res.status(400).json({
                msg: 'No se encontro el id indicado',
                error
            })
        }
        res.json(noteDB)
    } catch (error) {
        return res.status(400).json({
            msg: 'Ocurrio un error',
            error
        })
    }
})

// GET con todos los resultados
router.get('/notes', verifyAuth, async(req, res) => {
    const userId = req.user._id
    try {
        const noteDB = await Note.find({userId})
        res.json(noteDB)
    } catch (error) {
        return res.status(400).json({
            msg: 'Ocurrio un error',
            error
        })
    }
})

// DELETE eliminar registro
router.delete('/note/:id', async(req, res) => {
    const _id = req.params.id
    try {
        const noteDB = await Note.findByIdAndDelete({_id})
        res.json(noteDB)
    } catch (error) {
        return res.status(400).json({
            msg: 'Ocurrio un error',
            error
        })
    }
})

// PUT Actualizar registro
router.put('/note/:id', async(req, res) => {
    const _id = req.params.id
    const body = req.body
    try {
        const noteDB = await Note.findByIdAndUpdate({_id}, {body}, {new: true})
        res.json(noteDB)
    } catch (error) {
        return res.status(400).json({
            msg: 'Ocurrio un error',
            error
        })
    } 
})

// Exportación router
module.exports = router