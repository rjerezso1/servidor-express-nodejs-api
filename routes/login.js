import express from 'express'
const router = express.Router()

const jwt = require('jsonwebtoken')

import User from '../models/user'

// Encriptar contraseña
const bcrypt = require('bcrypt')
const saltRounds = 10

router.post('/', async(req, res) => {
    const body = req.body
    try {
        // Evaluando el email
        const userDB = await User.findOne({email: body.email})
        if (!userDB) {
            return res.status(400).json({
                message: 'Email incorrecto',
            }) 
        }
        // Evaluando la contraseña
        if (!bcrypt.compareSync(body.pass, userDB.pass)) {
            return res.status(400).json({
                message: 'Contraseña incorrecta',
                error
            })
        }
        // Generando el token
        const token = jwt.sign({
            data: userDB
        }, '$RTUDFFDQwos$dpo', { expiresIn: 60 * 60 * 24 * 30})

        res.json({
            userDB,
            token
        })
    } catch (error) {
        return res.status(400).json({
            message: 'Ocurrio un error',
            error
        })
    }
})


module.exports = router