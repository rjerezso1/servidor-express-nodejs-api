import express from 'express'
const router = express.Router()

import User from '../models/user'

const { verifyAuth, verifyAdmin } = require('../middlewares/authentication')

// Encriptar contraseña
const bcrypt = require('bcrypt')
const saltRounds = 10

// Filtrar campos de PUT
const _ = require('underscore')

// POST
router.post('/new-user', async(req, res) => {
    const body = {
        name: req.body.name,
        email: req.body.email,
        role: req.body.role,
        pass: bcrypt.hashSync(req.body.pass, saltRounds)
    }
    try {
        const userDB = await User.create(body)
        res.json(userDB)
    } catch (error) {
        return res.status(500).json({
            message: 'Ocurrio un error',
            error
        })
    }
})

// PUT
router.put('/user/:id', [verifyAuth, verifyAdmin], async(req, res) => {
    const _id = req.params.id
    // pick sirve para que los usuarios no puedan añadir propiedades al objeto como hacerse admin
    const body = _.pick(req.body, ['name', 'email', 'pass', 'active'])
    if (body.pass) {
        body.pass = bcrypt.hashSync(req.body.pass, saltRounds)
    }
    try {
        const userDB = await User.findByIdAndUpdate(
            _id, 
            body,
            {new: true, runValidators: true})
        return res.json(userDB)    
    } catch (error) {
        return res.status(400).json({
            message: 'Ocurrio un error',
            error
        })
    }
})

// Exportación router
module.exports = router