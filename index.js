import express from 'express'
import morgan from 'morgan'
import cors from 'cors'
import path from 'path'
import mongoose from 'mongoose'

const app = express()

// Conexión a DB mongoose
const uri = 'mongodb://localhost:27017/test'
const options = {
    autoIndex: false,
    maxPoolSize: 10,
    serverSelectionTimeoutMS: 5000,
    socketTimeoutMS: 45000,
    family: 4
  }
mongoose.connect(uri, options).then(() => { 
    console.log('Connected to mongoDB') },
    err => { err }
)


app.use(morgan('tiny'))
app.use(cors())
app.use(express.json())
// application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }))

//Rutas js
app.use('/api', require('./routes/note'))
app.use('/api', require('./routes/user'))
app.use('/login', require('./routes/login'))

// Middleware para VueJS router mode history
const history = require('connect-history-api-fallback')
app.use(history())
app.use(express.static(path.join(__dirname, 'public')))

// Si se sube a hosting tienen esta variable para los puertos (3000 para local)
app.set('port', process.env.PORT || 3000)

app.listen(app.get('port'), function () {
    console.log('Listening on port:', app.get('port'))
})