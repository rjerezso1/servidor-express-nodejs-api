import mongoose from 'mongoose'
const Schema = mongoose.Schema

const uniqueValidator = require('mongoose-unique-validator')

const roles = {
    values: ['ADMIN', 'USER'],
    message: '{VALUE} Rol no válido'
}

const userSchema = new Schema({
    name: {type: String, required: [true, 'Nombre requerido']},
    email: {
        type: String, 
        required: [true, 'Email requerido'],
        unique: true
    },
    pass: {type: String, required: [true, 'Contraseña requerida']},
    date: {type: Date, default: Date.now},
    role: {type: String, default: 'USER', enum: roles},
    active: {type: Boolean, default: true}
})

userSchema.plugin(uniqueValidator, { message: 'El email ya esta en uso'})

// ocultamos la contraseña
userSchema.methods.toJSON = function () {
    const obj = this.toObject()
    delete obj.pass
    return obj
}

// Convertir a un modelo
const User = mongoose.model('User', userSchema)

export default User
